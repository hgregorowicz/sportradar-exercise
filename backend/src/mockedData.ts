import { Simulation, SimulationState } from "src/dataTypes/simulation";

export const simulations: Simulation[] = [
  {
    id: 1,
    name: "Katar 2023",
    state: "WAITING",
    matches: [
      {
        id: 1,
        Germany: 0,
        Poland: 0,
      },
      {
        id: 2,
        Brazil: 0,
        Mexico: 0,
      },
      {
        id: 3,
        Argentina: 0,
        Uruguay: 0,
      },
    ],
  },
];
