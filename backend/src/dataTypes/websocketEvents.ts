import { Simulation } from "src/dataTypes/simulation";

export interface ServerClientEvents {
  initialData: (payload: Simulation[]) => void;
  refresh: (payload: Simulation[]) => void;
}

export interface ClientServerEvents {
  buttonClick: (payload: number) => void;
}
