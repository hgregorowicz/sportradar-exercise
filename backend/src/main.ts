import { NestFactory } from "@nestjs/core";

import { SimulationsModule } from "src/simulations/simulations.module";

async function bootstrap() {
  const app = await NestFactory.create(SimulationsModule);
  app.enableCors({
    origin: "http://localhost:3000",
  });
  await app.listen(5000);
}
bootstrap();
