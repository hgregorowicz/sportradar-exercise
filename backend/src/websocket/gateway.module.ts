import { Gateway } from "src/websocket/gateway";
import { Module } from "@nestjs/common";

import { SimulationsService } from "src/simulations/simulations.service";

@Module({
  providers: [Gateway, SimulationsService],
  exports: [Gateway],
})
export class SimulationsModule {}
