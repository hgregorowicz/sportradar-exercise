import { OnModuleInit } from "@nestjs/common";
import {
  MessageBody,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from "@nestjs/websockets";
import { Server } from "socket.io";

import {
  ClientServerEvents,
  ServerClientEvents,
} from "src/dataTypes/websocketEvents";
import { simulations } from "src/mockedData";
import { SimulationsService } from "src/simulations/simulations.service";

@WebSocketGateway({
  cors: {
    origin: "http://localhost:3000",
  },
})
export class Gateway implements OnModuleInit {
  constructor(private readonly SimulationsService: SimulationsService) {}

  @WebSocketServer()
  server: Server<ClientServerEvents, ServerClientEvents>;

  onModuleInit() {
    this.server.on("connection", (socket) => {
      this.server.to(socket.id).emit("initialData", simulations);
    });
  }

  @SubscribeMessage("buttonClick")
  onMessage(@MessageBody() id: number) {
    this.SimulationsService.handleUserAction(id, this.server);
  }
}
