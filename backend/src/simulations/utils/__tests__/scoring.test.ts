import { SimulationState } from "src/dataTypes/simulation";
import {
  getAllTeams,
  getRandomTeam,
  resetScores,
} from "src/simulations/utils/scoring";

describe("getAllTeams", () => {
  it("should return all teams from all matches in simulation", () => {
    const mockSimulation = {
      id: 1,
      name: "Katar 2023",
      state: "WAITING" as SimulationState,
      matches: [
        {
          id: 1,
          Germany: 0,
          Poland: 0,
        },
        {
          id: 2,
          Brazil: 0,
          Mexico: 0,
        },
      ],
    };
    const teams = getAllTeams(mockSimulation);
    expect(teams).toStrictEqual(["Germany", "Poland", "Brazil", "Mexico"]);
  });

  it("should be empty if simulation has no matches", () => {
    const mockSimulation = {
      id: 1,
      name: "Katar 2023",
      state: "WAITING" as SimulationState,
      matches: [],
    };
    const teams = getAllTeams(mockSimulation);
    expect(teams).toStrictEqual([]);
  });
});

describe("getRandomTeam", () => {
  it("should return one of available teams", () => {
    const teams = ["Germany", "Poland", "Brazil", "Mexico"];
    const random = getRandomTeam(teams);
    expect(teams.includes(random)).toBe(true);
  });
});

describe("resetScores", () => {
  it("should set all teams' scores to 0", () => {
    const mockSimulation = {
      id: 1,
      name: "Katar 2023",
      state: "WAITING" as SimulationState,
      matches: [
        {
          id: 1,
          Germany: 3,
          Poland: 1,
        },
        {
          id: 2,
          Brazil: 0,
          Mexico: 2,
        },
      ],
    };

    const resetSimulation = {
      id: 1,
      name: "Katar 2023",
      state: "WAITING",
      matches: [
        {
          id: 1,
          Germany: 0,
          Poland: 0,
        },
        {
          id: 2,
          Brazil: 0,
          Mexico: 0,
        },
      ],
    };
    resetScores(mockSimulation);
    expect(mockSimulation).toStrictEqual(resetSimulation);
  });
});
