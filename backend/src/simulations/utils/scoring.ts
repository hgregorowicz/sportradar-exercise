import { Simulation } from "src/dataTypes/simulation";

export const getAllTeams = (simulation: Simulation): string[] => {
  const teams = [];
  for (const match of simulation.matches) {
    const { id, ...teamsWithScores } = match;
    teams.push(...Object.keys(teamsWithScores));
  }
  return teams;
};

export const getRandomTeam = (teams: string[]): string =>
  teams[Math.floor(Math.random() * teams.length)];

export const score = (simulation: Simulation): void => {
  const team = getRandomTeam(getAllTeams(simulation));
  const match = simulation.matches.find((match) =>
    Object.keys(match).includes(team),
  );
  match[team] = match[team] + 1;
};

export const resetScores = (simulation: Simulation): void => {
  const clearMatches = [];
  for (const match of simulation.matches) {
    const { id, ...teamsWithScores } = match;
    const clearMatch = { id };
    for (const team in teamsWithScores) {
      clearMatch[team] = 0;
    }
    clearMatches.push(clearMatch);
  }
  simulation.matches = clearMatches;
};
