import { Injectable } from "@nestjs/common";
import { Server } from "socket.io";

import { Simulation, SimulationState } from "src/dataTypes/simulation";
import { simulations } from "src/mockedData";
import { resetScores, score } from "src/simulations/utils/scoring";

let interval: NodeJS.Timeout;
let timeout: NodeJS.Timeout;

@Injectable()
export class SimulationsService {
  handleUserAction(id: number, server: Server): void {
    const simulation = simulations.find((sim) => sim.id === id);

    if (simulation.state === "WAITING") {
      this.startSimulation(simulation, server);
    } else if (simulation.state === "RUNNING") {
      this.stopSimulation(simulation, server);
    } else {
      this.restartSimulation(simulation, server);
    }
  }

  refreshData = (server: Server): void => {
    server.emit("refresh", simulations);
  };

  startSimulation(simulation: Simulation, server: Server): void {
    simulation.state = "RUNNING";
    this.refreshData(server);

    interval = setInterval(() => {
      score(simulation);
      this.refreshData(server);
    }, 10000);

    timeout = setTimeout(() => {
      clearInterval(interval);
      simulation.state = "FINISHED";
      this.refreshData(server);
    }, 90100);
  }

  stopSimulation(simulation: Simulation, server: Server): void {
    clearInterval(interval);
    clearTimeout(timeout);
    simulation.state = "FINISHED";
    this.refreshData(server);
  }

  restartSimulation(simulation: Simulation, server: Server): void {
    resetScores(simulation);
    simulation.state = "WAITING";
    this.refreshData(server);
  }
}
