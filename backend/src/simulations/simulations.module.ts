import { Module } from "@nestjs/common";

import { SimulationsService } from "src/simulations/simulations.service";
import { Gateway } from "src/websocket/gateway";

@Module({
  providers: [Gateway, SimulationsService],
  exports: [SimulationsService],
})
export class SimulationsModule {}
