import { Test, TestingModule } from "@nestjs/testing";
import { Server } from "socket.io";

import { SimulationsService } from "src/simulations/simulations.service";

jest.mock("socket.io");

describe("SimulationsService", () => {
  let service: SimulationsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SimulationsService],
    }).compile();

    service = module.get<SimulationsService>(SimulationsService);
  });

  it("should be defined", () => {
    expect(service).toBeDefined();
  });

  describe("refreshData", () => {
    const server = new Server();
    jest.spyOn(server, "emit").mockImplementation(() => true);

    it("should call websocket emit function", () => {
      service.refreshData(server);
      expect(server.emit).toHaveBeenCalledTimes(1);
    });
  });
});
