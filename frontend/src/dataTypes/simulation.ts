export interface Simulation {
  id: number;
  name: string;
  state: SimulationState;
  matches: Match[];
}

export type SimulationState = "WAITING" | "RUNNING" | "FINISHED";

export interface Match {
  id: number;
  [teamName: string]: number;
}
