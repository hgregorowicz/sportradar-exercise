import { WebsocketContext, socket } from "./contexts/WebsocketContext";
import { Simulations } from "./components/Simulations";

function App() {
  return (
    <div className="App">
      <WebsocketContext.Provider value={socket}>
        <Simulations />
      </WebsocketContext.Provider>
    </div>
  );
}

export default App;
