import { Match } from "../dataTypes/simulation";

export const getTotalGoals = (matches: Match[]) =>
  matches.reduce(
    (totalGoals, { id, ...teams }) =>
      totalGoals +
      Object.values(teams).reduce((matchGoals, goals) => matchGoals + goals, 0),
    0
  );
