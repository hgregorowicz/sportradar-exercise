import { Match } from "../../dataTypes/simulation";
import { getTotalGoals } from "../getTotalGoals";

describe("getTotalGoals", () => {
  const mockSimulation = {
    id: 1,
    name: "Katar 2023",
    state: "WAITING",
    matches: [
      {
        id: 1,
        Germany: 2,
        Poland: 1,
      },
      {
        id: 2,
        Brazil: 0,
        Mexico: 3,
      },
    ] as Match[],
  };

  it("should return correct goals number", () => {
    const sum = getTotalGoals(mockSimulation.matches);
    expect(sum).toBe(6);
  });
});
