import React from "react";
import { WebsocketContext } from "../contexts/WebsocketContext";
import { SimulationState } from "../dataTypes/simulation";

interface Props {
  state: SimulationState;
  simulationId: number;
}

export const SimulationButton: React.FC<Props> = ({ state, simulationId }) => {
  const socket = React.useContext(WebsocketContext);

  const buttonText: Record<SimulationState, string> = {
    WAITING: "Start",
    RUNNING: "Finish",
    FINISHED: "Restart",
  };

  const handleClick = () => {
    socket.emit("buttonClick", simulationId);
  };

  return <button onClick={handleClick}>{buttonText[state]}</button>;
};
