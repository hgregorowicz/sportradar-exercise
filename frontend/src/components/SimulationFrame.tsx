import { Simulation } from "../dataTypes/simulation";
import { getTotalGoals } from "../utils/getTotalGoals";
import { MatchItem } from "./MatchItem";
import { SimulationButton } from "./SimulationButton";

const simulationStyle: React.CSSProperties = {
  width: "30vw",
  border: "1px solid black",
  borderRadius: "5px",
  padding: "10px",
  margin: "10px",
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
};

const marginStyle: React.CSSProperties = {
  margin: "8px",
};

interface Props {
  data: Simulation;
}
export const SimulationFrame: React.FC<Props> = ({ data }) => {
  return (
    <div style={simulationStyle}>
      <div style={marginStyle}>{data.name}</div>
      <div style={marginStyle}>
        <SimulationButton state={data.state} simulationId={data.id} />
      </div>
      {data.matches.map((match) => (
        <MatchItem match={match} key={match.id} />
      ))}
      <div style={marginStyle}>Total goals: {getTotalGoals(data.matches)} </div>
    </div>
  );
};
