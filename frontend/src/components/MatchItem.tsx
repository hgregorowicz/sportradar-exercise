import { Match } from "../dataTypes/simulation";

const cardStyle: React.CSSProperties = {
  padding: 8,
  margin: 4,
  border: "solid black 1px",
  display: "flex",
  justifyContent: "space-between",
  width: "90%",
};

interface Props {
  match: Match;
}

export const MatchItem: React.FC<Props> = ({ match }) => {
  const { id, ...teams } = match;

  const teamsString = Object.keys(teams).join(" vs ");
  const scoreString = Object.values(teams).join(" : ");

  return (
    <div style={cardStyle}>
      <div> {teamsString} </div>
      <div> {scoreString} </div>
    </div>
  );
};
