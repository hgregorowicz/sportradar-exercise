import React from "react";
import { WebsocketContext } from "../contexts/WebsocketContext";
import { Simulation } from "../dataTypes/simulation";
import { SimulationFrame } from "./SimulationFrame";

export const Simulations = () => {
  const socket = React.useContext(WebsocketContext);
  const [simulations, setSimulations] = React.useState<Simulation[]>([]);
  React.useEffect(() => {
    socket.on("initialData", (data: Simulation[]) => {
      setSimulations(data);
    });

    socket.on("refresh", (data: Simulation[]) => {
      console.log(data);
      setSimulations(data);
    });

    return () => {
      socket.off("initialData");
    };
  }, []);
  return (
    <div>
      {simulations.map((simulation) => (
        <SimulationFrame data={simulation} key={simulation.id} />
      ))}
    </div>
  );
};
