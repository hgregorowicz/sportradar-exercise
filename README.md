# Sportradar exercise

### Description

React & NestJS app simulating football matches.

### Installation

Make sure you have docker installed, then type following commands:

```
docker-compose build
docker-compose up
```

App should be running on localhost:3000

If you have any problems with docker, you can run the project manually by typing:

```
cd backend
npm i
npm start
```

And the same process for frontend in a seperate terminal.

### Project status

I no longer had time to write a good set of tests, I only managed to write some unit tests.

Best regards,

Hania
